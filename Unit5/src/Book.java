
public class Book {

	private String name;
	private Author author;
	private double price;
	private int qtyInStock;
	
	public Book(String n, Author a, double p) {
		name = n;
		author = a;
		price = p;
	}
	
	public Book(String n, Author a, double p, int x) {
		name = n;
		author = a;
		price = p;
		qtyInStock = x;
	}

	protected double getPrice() {
		return price;
	}

	protected void setPrice(double price) {
		this.price = price;
	}

	protected int getQtyInStock() {
		return qtyInStock;
	}

	protected void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}

	protected String getName() {
		return name;
	}

	protected Author getAuthor() {
		return author;
	}
	
	@Override
	public String toString() {
		return name + " by " + author.toString();
	}
	
	public String getAuthorName() {
		return author.getName();
	}
	
	public String getAuthorEmail() {
		return author.getEmail();
	}
	
	public char getAuthorGender() {
		return author.getGender();
	}
}
