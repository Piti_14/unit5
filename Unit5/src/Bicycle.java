
public class Bicycle {
	int cadence;
	int speed;
	int gear;
	public static final int MAX_VELOCITY = 100;
	
	public Bicycle() {
		cadence = 0;
		speed = 0;
		gear = 1;
	}
	
	public Bicycle(int initialCadence, int initialSpeed, int initialGear) {
		cadence = initialCadence;
		speed = initialSpeed;
		gear = initialGear;
	}
	void changeCadence(int newValue) {
		cadence = newValue;
	}
	void changeGear(int newValue) {
		gear = newValue;
	}
	void speedUp(int increment) {
		speed = speed + increment;
		if(speed > MAX_VELOCITY) {
			speed = MAX_VELOCITY;
		}
	}
	void applyBrakes(int decrement) {
		speed = speed - decrement;
		if(speed < 0) {
			speed = 0;
		}
	}
	void printStates(){
		System.out.println("cadence:" + cadence + " speed:" + speed + " gear:" + gear);    }
}
