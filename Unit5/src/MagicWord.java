
public class MagicWord {
	
	public static final String[] WORDS = {"Home", "Bathroom", "Nightmare", "Warlords", "Bob Squarepants", 
	"Harry Potter and the Philosopher's Stone"};
	
	private boolean[] b;
	private String magicWord;
	private boolean win, firstAttempt;
	private int lastingLetters;
	
	public MagicWord() {
		magicWord = WORDS[(int)(Math.random() * WORDS.length)].toUpperCase();
		b = new boolean[magicWord.length()];
		win = false;
		firstAttempt = true;
		
		for(int i = 0; i < b.length; i++) {
			if(specialCharacter(i)) {
				b[i] = true;
			} else {
				b[i] = false;
				lastingLetters++;
			}
		}
 	}

	private boolean specialCharacter(int i) {
		return magicWord.charAt(i) == ' ' || magicWord.charAt(i) == '\'';
	}

	public void check(char l) {
		boolean contains = false;
		for(int i = 0; i < b.length; i++) {
			if(l == magicWord.charAt(i)) {
				b[i] = true;
				contains = true;
				lastingLetters--;
			}
		}
		if(contains) {
			System.out.println("Yay! That letter is in the hidden word!");
		} else {
			System.out.println("There are no '" + l + "' in this hidden word.");
		}
		if(lastingLetters <= 0) {
			win = true;
			WordGuess.attempt--;
			System.out.println("Congratulations! You guessed the word!\n It was:" + magicWord);
		}
	}
	
	public void checkSolution(String s) {
		if(magicWord.equals(s)) {
			System.out.println("Congratulations! You guessed the word!\n It was:" + magicWord);
			win = true;
			for(int i = 0; i < b.length; i++) {
				b[i] = true;
			}
			WordGuess.attempt --;
		} else {
			System.out.println("Oh, that was close! Try again");
		}
	}
	
	@Override
	public String toString() {
		String s = "";
		if(firstAttempt) {
			for(int i = 0; i < magicWord.length(); i++) {
				if(specialCharacter(i)) {
					s += magicWord.charAt(i);
				} else {
					s += "_ ";
				}
			}
			firstAttempt = false;
			return s;
		} else {
			for(int i = 0; i < magicWord.length(); i++) {
				if(b[i]) {
					s += magicWord.charAt(i);
				} else if(specialCharacter(i)) {
						s += magicWord.charAt(i);
				} else {
					s += "_ ";
				}
			}
			return s;
		}
	}
	
	public boolean getWinCondition() {
		return win;
	}
}
