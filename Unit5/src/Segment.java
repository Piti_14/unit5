
public class Segment {
	
	private Point startPoint, endPoint;
	
	public Segment(){
		startPoint = new Point();
		endPoint = new Point ();
	}
	
	public Segment(Point p1, Point p2){
		startPoint = p1;
		endPoint = p2;
	}
	
	public double module() {
		double distance, c1, c2;
		c1 = Math.pow((startPoint.getX() - endPoint.getX()),2);
		c2 = Math.pow((startPoint.getY() - endPoint.getY()),2);
		distance = Math.sqrt(c1 + c2);
		return distance;
	}
	@Override
	public String toString() {
		return "(" + startPoint + " - " + endPoint + ")";
	}
	
	public void setOffset(int offX, int offY) {
				startPoint.setOffset(offX, offY);
				endPoint.setOffset(offX, offY);
	}
	
	public void setStartPoint(int x, int y) {
		startPoint.setX(x);
		startPoint.setY(y);
	}
	
	public void setEndPoint(int x, int y) {
		endPoint.setX(x);
		endPoint.setY(y);
	}
	
	public Point getStartPoint() {
		return startPoint;
	}
	
	public Point getEndPoint() {
		return endPoint;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
