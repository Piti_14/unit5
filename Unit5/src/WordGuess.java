import java.util.Scanner;

public class WordGuess {

	static MagicWord word;
	static String letter;
	static char l;
	static Scanner value = new Scanner(System.in);
	public static int attempt;
	
	public static void main(String[] args) {
		
		attempt = 1;
		word = new MagicWord();
		System.out.println("Attempt " + attempt + "\nHidden Word: " + word.toString());

		while (!word.getWinCondition()) {

			askLetter(value);
			attempt++;
			System.out.println("Attempt " + attempt + "\n" + word.toString());
		}
	}

	private static void askLetter(Scanner input) {

		System.out.println("Enter one character or your word guess: ");
		letter = input.nextLine().toUpperCase();

		if (letter.length() > 1) {
			word.checkSolution(letter);
		} else {
			l = letter.charAt(0);
			if (isValid()) {
				word.check(l);
			} else {
				System.out.println("That's an invalid value. Please try again.");
				askLetter(input);
			}
		}
	}

	private static boolean isValid() {
		if (l < 'A' || l > 'Z') {
			return false;
		} else {
			return true;
		}
	}
}
