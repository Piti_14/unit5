
public class SwitchBoard {
	
	private Call[] calls;
	private int numCall;
	
	public SwitchBoard(int maxCalls) {
		calls = new Call[maxCalls];
		numCall = 0;
	}
	
	public void registerCall(Call call) {
		calls[numCall] = call;
		numCall++;
	}
	
	public void print(int i) {
		if(i >= 0 && i <= numCall) {
		calls[i].toString();
		} else {
			System.out.println("Not so many calls registered. There's no such call yet.");
		}
	}
	
	public void print() {
		for (int i = 0; i < numCall; i++) {
			calls[i].toString();	
		}
	}
	
	public int callCost(Call c) {
		int cost = 0;
		if(c.isLocal()) {
			cost = c.getDuration() * 15;
		} else if(c.getTimeBand() == 1) {
			cost = c.getDuration() * 20;
		} else if(c.getTimeBand() == 2) {
			cost = c.getDuration() * 25;
		} else if(c.getTimeBand() == 3) {
			cost = c.getDuration() * 30;
		} else {
			System.out.println("Error in the time band type. Please correct it to a defined value.");
		}
		return cost;
	}
	
	public int allCallsCost() {
		int totalCost = 0;
		for(int i = 0; i < numCall; i++) {
			totalCost += callCost(calls[i]);
		}
		return totalCost;
	}
}
