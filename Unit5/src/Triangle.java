
public class Triangle {
	private Point v1, v2, v3;
	private double firstLine, secondLine, thirdLine;
	
	public Triangle(int x1, int y1, int x2, int y2, int x3, int y3) {
		v1.setX(x1);
		v1.setY(y1);
		v2.setX(x2);
		v2.setY(y2);
		v3.setX(x3);
		v3.setY(y3);
		firstLine = Point.distance(v1, v2);
		secondLine = Point.distance(v1, v3);
		thirdLine = Point.distance(v2, v3);
	}
	
	public Triangle(Point p1, Point p2, Point p3) {
		v1 = p1;
		v2 = p2;
		v3 = p3;
	}
	
	@Override
	public String toString() {
		return "Triangle @ [" + v1.toString() + ", " + v2.toString() + ", " + v3.toString() + "]";
	}
	
	public double getPerimeter() {
		return firstLine + secondLine + thirdLine;
	}
	
	public void printType() {
		if(firstLine == secondLine && firstLine == thirdLine) {
			System.out.println("Equilateral");
		} else if(firstLine == secondLine || firstLine == thirdLine || secondLine == thirdLine) {
			System.out.println("Isosceles");
		} else {
			System.out.println("Escalene");
		}
	}
}
