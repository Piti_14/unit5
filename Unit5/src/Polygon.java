
public class Polygon {
	
	private Point[] points;
	
	public Polygon() {
		points = new Point[0];
	}
	
	public Polygon(Point[] a) {
		points = a;
	}
	
	@Override
	public String toString() {
		String result = "Polygon [ ";
		for(int i = 0; i <= points.length - 2; i++) {
			result += "(" + points[i].getX() + ", " + points[i].getY() + ") - ";
		}
		result += "(" + points[points.length - 1].getX() + ", " + points[points.length - 1].getY() + "]";
		
		return result;
	}
	
	public void setOffset(int offX, int offY) {
		for(int i = 0; i <= points.length; i++) {
			points[i].setOffset(offX, offY);
		}
	}
	
	public double perimeter() {
		
		double length = 0;
		for(int i = 1; i < points.length; i++) {
			Point p1 = points[i];
			Point p2 = points[i - 1];
			
			Segment s = new Segment(p1, p2);
			
			length += s.module();
		}
		Point last = points[points.length - 1];
		Point first = points[0];
		
		Segment s = new Segment(last, first);
		length += s.module();
		
		return length;		
	}
	
}
