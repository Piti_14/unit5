
public class Rectangle extends Point{
	
	private int width, height;
	/*Esta clase tiene tres atributos.
	 La base, la altura y el punto inicial, el cual obtendremos llamando 
	 al constructor de la superclase (Point)*/
	
	public Rectangle() {
		 super();
		 width = 0;
		 height = 0;
	}
	
	public Rectangle(Point p, int newWidth, int newHeight) {
		super(p.getX(), p.getY());
		width = newWidth;
		height = newHeight;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Width: " + width + "  Height: " + height;
	}
	
	public Rectangle(Segment s) {
		super(s.getStartPoint().getX(), s.getStartPoint().getY()); //Guardamos el atributo PUNTO_INICIAL. 
		Point p1 = s.getStartPoint();
		Point p2 = s.getEndPoint();
		
		width = p2.getX() - p1.getX();
		height = p2.getY() - p1.getY();
	}
	
	public int getArea() {
		return width * height;
	}
	
	public Point getPoint() {
		return new Point(getX(), getY());
	}
	
	public Point getTopLeftPoint() {
		
		int x = getX(); 
		int y = getY();
		Point topLeft = new Point(x, y + height);
		return topLeft;
	}
	
	public Point getTopRightPoint() {
		Point topRight = getTopLeftPoint();
		return new Point(topRight.getX() + width, topRight.getY());
	}
	
	public Point getBottomLeftPoint() {
		 return getPoint();
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Point getBottomRightPoint() {
		return new Point(getPoint().getX() + width, getPoint().getY());		
	}
	
	public String printPoints() {
		return "- Top Left: " + getTopLeftPoint() + "\n- Top Right: " + getTopRightPoint() + "\n- Bottom Left: " + getBottomLeftPoint()
		 + "\n- Bottom Rigth: " + getBottomRightPoint();
	}
	
	
	
}
