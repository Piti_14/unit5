
public class DNI {
	
	public static final char[] LETTERS = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S',
			'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
	
	private int number;
	private char letter;
	
	public DNI() {
		number = 0;
		letter = LETTERS[0];
	}
	
	public DNI(String s) {
		letter = s.charAt(s.length() - 1);
		number = Integer.parseInt(s.replaceAll("[^\\d]", ""));
		checkLetter();
	}
	
	public DNI(int n, char l) {
		number = n;
		letter = l;
		checkLetter();
	}
	
	public DNI(int n) {
		number = n;
		letter = LETTERS[number % 23];
	}
	
	private void checkLetter() {
		if(number >= 0) {
			if(LETTERS[number % 23] != letter) {
				number = -number;
			} else if(LETTERS[-number % 23] == letter) {
				number = -number;
			}
		}
	}
	
	protected int getNumber() {
		return number;
	}

	protected void setNumber(int number) {
		this.number = number;
		checkLetter();
	}

	protected char getLetter() {
		return letter;
	}
	
	@Override
	public String toString() {
		return "" + number + letter;
	}
	
	public String toFormattedString() {
		String n1, n2, n3;
		String n = "" + number;
		n1 = n.substring(0, 1);
		n2 = n.substring(2, 4);
		n3 = n.substring(5, 7);
		return n1 + "." + n2 + "." + n3 + "-" + letter;	
	}
	
	public boolean isCorrect() {
		if(number < 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public static char letterForDNI(int n) {
		return LETTERS[n % 23];
	}
	
	public static String NifForDNI(int i) {
		return new DNI(i).toString();
	}
}