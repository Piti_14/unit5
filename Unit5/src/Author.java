
public class Author {
	
	private String name, email;
	private char gender;
	
	public Author(String n, String mail, char gender) {
		name = n;
		email = mail;
		this.gender = gender;
	}

	protected String getEmail() {
		return email;
	}

	protected void setEmail(String email) {
		this.email = email;
	}

	protected String getName() {
		return name;
	}

	protected char getGender() {
		return gender;
	}
	
	@Override
	public String toString() {
		return name + "(" + gender +") at " + email; 
	}
}
