package WordGuessTwo;

public class Player {
	
	private String name;
	private int turn;
	private int attempts;
	private MagicWord magic;
	
	
	
	public Player(String name, int turn) {
		this.name = name;
		this.turn = turn;
		magic = new MagicWord();
		attempts = 0;
	}



	public String getName() {
		return name;
	}

	public int getTurn() {
		return turn;
	}

	public int getAttempts() {
		return attempts;
	}

	public MagicWord getMagic() {
		return magic;
	}
	
	public String getMagicString() {
		return magic.toString();
	}
	

	
	public void addAttempts() {
		attempts++;
	}
	
	public boolean checkAnswer(String answer) {
		magic.checkAnswer(answer);
		return magic.isWinner();
	}
	
	public boolean wins() {
		return magic.isWinner();
	}
}
