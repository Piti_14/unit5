package WordGuessTwo;

public class MagicWord {
	
	private static final String[] WORDS = {"theatre", "capital", "cinema", "parachute", "traveller"};
	private boolean[] guessed;
	private String hiddenWord;
	
	public MagicWord() {
		int numAlea = (int) (Math.random() * WORDS.length);
		hiddenWord = WORDS[numAlea];
		guessed = new boolean[hiddenWord.length()];
		for (int i=0; i<guessed.length; i++) {
			guessed[i] = false;
		}		
	}
	
	@Override
	public String toString()  {
		String s = "";
		for (int i = 0; i < guessed.length; i++) {
			if (guessed[i]) {
				s += hiddenWord.charAt(i)+" ";
			} else {
				s += "_ ";
			}
		}
		return s;
	}
	
	public void checkAnswer(String answer) {
		
		if (answer.length() == 1) {
			checkLetter(answer.toLowerCase().charAt(0));
		} else {
			checkWord(answer.toLowerCase());
		}
	}
	
	private void checkLetter(char answer) {
		
		for (int i = 0; i < hiddenWord.length(); i++) {
			if (hiddenWord.charAt(i) == answer) {
				guessed[i] = true;
			}
		}
	}
	
	private void checkWord(String answer) {
		
		if (hiddenWord.equals(answer)) {
			putAllGuessedToTrue();
		}
	}
	
	private void putAllGuessedToTrue() {
		
		for (int i = 0; i < guessed.length; i++) {
			guessed[i] = true;
		}
	}
	
	public boolean isWinner() {
		
		for (int i = 0; i < guessed.length; i++) {
			if (!guessed[i]) {
				return false;
			}
		}
		return true;
	}
}
