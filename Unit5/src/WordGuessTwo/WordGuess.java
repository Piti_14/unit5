package WordGuessTwo;

import java.util.Scanner;

public class WordGuess {
	
	private static Scanner scan = new Scanner(System.in);

	public static void main (String argv[]) {
	
		boolean gameOver = false;
		
		Player playerOne = readPlayerFromKeyboard(1);
		Player playerTwo = readPlayerFromKeyboard(2);
		Player currentPlayer = playerOne;
		
		while(!gameOver) {
			
			System.out.println(currentPlayer.getName() + " " + currentPlayer.getAttempts() + ": " + currentPlayer.getMagicString());
			String answer = scan.nextLine();
			
			currentPlayer.checkAnswer(answer);
			
			gameOver = isWinner(gameOver, currentPlayer, answer);
			System.out.println(currentPlayer.getMagicString()+"\n________________________\n");
			
			currentPlayer.addAttempts();
			if (currentPlayer.getTurn() == 1) {
				currentPlayer = playerTwo; 
			} else {
				currentPlayer = playerOne;
			}
		}
		
		scan.close();
	}

	private static boolean isWinner(boolean gameOver, Player currentPlayer, String answer) {
		if (currentPlayer.wins()) {
			System.out.println(currentPlayer.getName() + " is the winner with "+ currentPlayer.getAttempts() + " attempts");
			gameOver = true;
		}
		return gameOver;
	}

	private static Player readPlayerFromKeyboard(int turn) {
		
		System.out.println("Player "+ turn + ". Insert your name");
		String name = scan.nextLine();
		
		return new Player(name, turn);
	}
}
