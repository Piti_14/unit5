
public class MainClass4 {

	public static void main(String[] args) {
		
		Rectangle r = new Rectangle();
		System.out.println(r);
		r.moveTo(4, 4);
		//System.out.println(r);
		
		Point p = new Point(4,5);
		//Rectangle r2 = new Rectangle(p, 8, 6);
		//System.out.println(r2);
		
		Point p2 = new Point(6, 8);
		Segment s = new Segment(p, p2);
		Rectangle r3 = new Rectangle(s);
		System.out.println(r3);
		System.out.println(r3.getArea());
		
		//System.out.println(r3.getBottomRightPoint());
		//System.out.println(r3.getBottomLeftPoint());
		//System.out.println(r3.getTopLeftPoint());
		//System.out.println(r3.getTopRightPoint());
		System.out.println(r3.printPoints());
	}

}
