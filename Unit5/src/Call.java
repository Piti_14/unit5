
public class Call {
	
	private int sourceNumber, destinationNumber,timeBand, seconds;
	private boolean  local;
	
	protected boolean isLocal() {
		return local;
	}

	protected void setLocal(boolean local) {
		this.local = local;
	}

	public Call(int e, int r, int n, int s) {
		sourceNumber = e;
		destinationNumber = r;
		timeBand = n;
		local = true;
	}
	
	public Call(int e, int r, int n, int s, char callType) {
		sourceNumber = e;
		destinationNumber = r;
		timeBand = n;
		if(callType == 'l') {
			local = true;
		} else {
			local = false;
		}
	}
	
	@Override
	public String toString() {
		String s = "";
		if(local) {
			s += "Call type: local\n";
		} else {
			s += "Call type: provincial\n";
		}
		return s += "Source Number: " + sourceNumber + "\nDestination number: " + destinationNumber 
				+ "\nDuration: " + seconds + " seconds\nTime Band: " + timeBand;
	}
	
	protected int getTimeBand() {
		return timeBand;
	}

	protected void setTimeBand(int timeBand) {
		this.timeBand = timeBand;
	}

	protected int getSourceNumber() {
		return sourceNumber;
	}

	protected int getDestinationNumber() {
		return destinationNumber;
	}

	protected int getDuration() {
		return seconds;
	}
	
	
}
