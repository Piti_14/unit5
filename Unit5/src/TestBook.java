
public class TestBook {
	
	static Book[] books = new Book[5];
	static Author a, b, c, d, e;
	static Book book1, book2, book3, book4, book5;
	
	public static void main(String[] args) {
		
		a = new Author("Lewis", "lewis314@hotmail", 'm');
		book1 = new Book("Alice in Wonderland", a, 29.90);
		
		System.out.println(book1.getAuthor().getName());
		System.out.println(book1.getAuthor().getEmail());
		
		b = new Author("Patrick Rothfuss", "patrickr@gmail.com", 'm');
		c = new Author("Miguel de Cervantes", "Cervantes@hotmail.com", 'm');
		d = new Author("J.K. Rowling", "jkr@gmail.com", 'f');
		e = new Author("Stephen King", "King_stephen@hotmail.com", 'm');
		book2 = new Book("The name of the wind", b, 49,90);
		book3 = new Book("Don Quijote de la mancha", c, 19,90);
		book4 = new Book("Harry Potter y el misterio del Príncipe Mestizo", d, 25);
		book5 = new Book("In the tall grass", e, 39,90);
	}

}
