import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MyPolynomial {
	private double[] coeffs;
	private int degree;
	
	public MyPolynomial(double... coeffs) {
		this.coeffs = coeffs;
		degree = coeffs.length - 1;
	}
	
	public MyPolynomial(String filename) {
		Scanner in = null;
		
		try {
			in = new Scanner(new File(filename));//open file
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		degree = in.nextInt(); //read the degree
		coeffs = new double[degree + 1]; //allocate the array
		for(int i = 0; i < coeffs.length; ++i) {
			coeffs[i] = in.nextDouble();
		}
	}
	
	public int getDegree() {
		return degree;
	}
	
	@Override
	public String toString() {
		String s = "";
		
		for(int i = coeffs.length - 1; i > 1; i--) {
			s += "" + coeffs[i] + "x^" + i + " + ";
		}
		s += "" + coeffs[1] + "x + " + coeffs[0];
		return s;
	}
	
	
}

