
public class MainClass3 {

	public static void main(String[] args) {
		
		final Segment s1 = new Segment();
		
		s1.setStartPoint(4, 5);
		s1.setEndPoint(6, 8);
		
		s1.setOffset(4, 4);
		
		System.out.println(s1.toString());
		System.out.println(s1.module());
	}

}
