
public class Point {
	
	private int x, y;
	
	public Point() {
		x = 0;
		y = 0;
	}
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	public void moveTo(int a, int b) {
		x = b;
		y = a;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public void setX(int a) {
		x = a;
	}
	public void setY(int b) {
		y = b;
	}
	public void setOffset(int offX, int offY) {
		x += offX;
		y += offY;
	}
	public static double distance(Point p1, Point p2) {
		double c1 = p2.getX() - p1.getX();
		double c2 = p2.getY() - p1.getY();
		double h = Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));
		return h;
	}
}
